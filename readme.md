#RUN, an infinite runner for Gambuino#

Run is an infinite runner game for the portable arduino-based console Gambuino. 
The goal is to make the highest score possible without dying.

The buttons are :

- A : Jump

- C : Return to main menu