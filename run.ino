//imports the SPI library (needed to communicate with Gamebuino's screen)
#include <SPI.h>

//imports the Gamebuino library
#include <Gamebuino.h>
#include <EEPROM.h>



#include <avr/pgmspace.h>

#include "part.h"

//creates a Gamebuino object named gb
Gamebuino gb;

#define HSC_OFFSET 2
void writeHSC(char name[4], int score,char pos);
unsigned int readHSCScore(char pos);
void readHSCName(char* buffer,char pos);

#define BLOCK_COUNT 5
#define GRAV 7
#define PLAYER_JUMP -40
#define DEFAULT_BONUS -50
#define MAX_JUMPS 5

//#define DEBUG

/////////HEADERS/////////////////
void generateBlock();
void updateBlocks();
void drawBlocks();

void updatePlayer();
void drawPlayer();
void drawHud();

void inputsGame();
void inputsMenu();

void drawMenu();

void generateBonus();
void updateBonus();
void drawBonus();

void setGameOver();

void initGameOver();

void menuLoop();
void gameLoop();
void gameOverLoop();
void highScoreLoop();

void menuPlayItem();
void drawGameOver();

void initHighScore();
void drawHighScore();

char drawOffsetX = 0;
char drawOffsetY = 0;

char _speed = 2; // Number of pixels update

#define MSG_COUNT 5
const char nice[] PROGMEM = "NICE";
const char cool[] PROGMEM = "COOL";
const char awesome[] PROGMEM = "AWESOME";
const char exellent[] PROGMEM = "EXELLENT";
const char godlike[] PROGMEM = "GOD LIKE";

char highScoresNames[3][4];
unsigned int highScoresScore[3];

const char* const Messages[] PROGMEM = {nice,cool,awesome,exellent,godlike};
int PROGMEM MessageTreshold[] = {100,200,300,400,500};
int messageTimer = 0;
char message[25];


const byte runScreen[] PROGMEM = {64,30,
B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,
B00001010,B10101010,B10101010,B10101010,B00001010,B00001000,B00000010,B10000000,
B00000101,B01010101,B01010101,B01010101,B00010101,B00010100,B00000101,B01000000,
B00001010,B10101010,B10101010,B10101010,B00101010,B00101010,B00000010,B10000000,
B00000000,B00000000,B01010100,B00010100,B00010101,B00010101,B00000101,B01000000,
B00000000,B00101000,B00101000,B00001010,B00001010,B00101010,B00001010,B10000000,
B00000000,B01010000,B00010100,B00010100,B00000100,B01000101,B00010000,B00000000,
B00000000,B10100000,B00101000,B00101000,B00001010,B00101010,B10001000,B00000000,
B00000000,B01010000,B01010000,B00010100,B00010100,B01000101,B00010000,B00000000,
B00000000,B10100010,B10000000,B00101000,B00001000,B10000010,B10100000,B00000000,
B00000001,B01010101,B00000000,B01010000,B00010100,B01000101,B00010000,B00000000,
B00000000,B10101010,B10000000,B00101000,B00101000,B10000010,B10100000,B00000000,
B00000001,B01000101,B01000000,B01010000,B01010001,B00000001,B01000000,B00000000,
B00000010,B10000010,B10000000,B10101000,B10100000,B10000010,B10000000,B00000000,
B00000001,B01000001,B01010100,B01010101,B01010101,B00000001,B01000000,B00000000,
B00000010,B10000000,B10101000,B00101010,B10101010,B00000000,B10000000,B00000000,
B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,
B01010100,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,B00000000,
B10101010,B00000000,B00000000,B00000000,B00000000,B00000000,B00101010,B10101010,
B00000000,B00101010,B10101000,B00000000,B00000000,B00000000,B01010101,B01010101,
B00000000,B00010101,B01010000,B00000000,B00000000,B00000000,B00101010,B10101010,
B00100000,B00000000,B00000000,B00000000,B00000000,B00000000,B01010101,B01010101,
B00000100,B01011100,B00000010,B10101010,B10101010,B00000000,B00000000,B00000000,
B10000001,B00011100,B00000001,B01010101,B01010100,B00000000,B00000000,B00000000,
B00101000,B01011100,B00000000,B00000000,B00000000,B10101010,B10000000,B00000000,
B00000010,B00000000,B00000000,B00000000,B00000000,B01010101,B01011111,B11111111,
B11111111,B11111111,B11111111,B11100000,B00000000,B00000000,B00011111,B11111111,
B11111111,B11111111,B11111111,B11100000,B00000000,B00000000,B00011111,B11111111,
B11111111,B11111111,B11111111,B11100111,B11111111,B11111111,B10011111,B11111111,
B11111111,B11111111,B11111111,B11100111,B11111111,B11111111,B10011111,B11111111,
};


// Define a block on the screen
struct block{
  int x; // x position of the top left corner
  unsigned char y; // y position of the top left corner
  int w; // width of the block. If = -1, the block is dead
  signed char h; // height of the block
};

struct Player{
 char x;
 char y;
 char s;
 char jump;
 int bonusScore;
 char bonusMult;
 int vy;
};

enum States{
	MENU = 0,
	GAME = 1,
	GAMEOVER = 2,
	HIGHSCORE = 3
};

States GameState = MENU;

void (*gameStates[4]) ();



#define BONUS_COUNT 2
#define BONUS_WIDTH 3
#define BONUS_HEIGHT 5
enum BonusType{
	MORE_JUMPS = 1,
	MULTI = 2,
	SCOREBONUS = 3,
	SLOW = 4,
	BONUS_MAX = 4,
	NONE = 0
};
struct Bonus{
	int x;
	char y;
	enum BonusType type;
};


unsigned int score = 0;
// Collection of blocks
block blocks[BLOCK_COUNT]; 
Player player;
Bonus bonuses[BONUS_COUNT];

char initialize = 0;
void _init(){

	gb.battery.show = 0;
  for (char i = 0; i<BLOCK_COUNT; i++)
    blocks[i].w = -1;
  
  blocks[0].x = 0;
  blocks[0].y = 40;
  blocks[0].h = 8;
  blocks[0].w = 1000;
  
  blocks[1].x = 1500;
  blocks[1].y = 40;
  blocks[1].h = 8;
  blocks[1].w = 500;
  
  player.x = 5;
  player.y = 30;
  player.s = 2;
  player.jump = 0;
  player.vy = 0;
  player.bonusMult = 1;
  player.bonusScore = DEFAULT_BONUS;
  
  score = 0;
  _speed = 20;
  
  drawOffsetX = 0;
  drawOffsetY = 0;
  
  initialize = 0;
  
  partInit();
  
  for(int i = 0; i < BONUS_COUNT; i++)
	{
		bonuses[i].type = NONE;
	}
  
  sprintf(message,"GO GO GO !");
  messageTimer = 40;
  generateBlock();
  generateBlock();
  generateBlock();
}

void initGameOver(){
	_init();
	GameState = GAME;
}

void setup(){ // Da setup
  gb.begin();
  gb.titleScreen(F("Run"),runScreen);
  //writeHSC("BOB",12345,0);
  //writeHSC("VLD",99999,1);
  //writeHSC("AAA",11111,2);
  initHighScore();
  gb.pickRandomSeed();
  
  gameStates[0] = menuLoop;
  gameStates[1] = gameLoop;
  gameStates[2] = gameOverLoop;
  gameStates[3] = highScoreLoop;
  
  initMainMenu();
  _init();
  
}

int speedCounter = 0;
void loop(){ // Da main loop
  // put your main code here, to run repeatedly:
  if(gb.update())
  { 
    
	
	(*gameStates[GameState]) ();
	
	
    
  }
}


// SUB LOOPS
///////////////////////////////////////////
//////////////////MENU/////////////////////
///////////////////////////////////////////
const char m_play[] PROGMEM = "PLAY";
const char m_options[] PROGMEM = "OPTIONS";
const char m_quit[] PROGMEM = "QUIT";

const char* const menuStrings[] PROGMEM = {m_play,m_options,m_quit};


void menuLoop(){
	inputsMenu();
	
	drawMenu();
}

char index;

void (*menuActions[10]) ();
void menuPlayItem(){
	GameState = GAME;
	_init();
}
void menuOptionItem(){}
void menuOptionQuit(){}

char menuMaxIndex;

void initMainMenu(){
	partInit();
	GameState = MENU;
	menuActions[0] = menuPlayItem;
	menuActions[1] = menuOptionItem;
	menuActions[2] = menuOptionQuit;
	
	menuMaxIndex = 2;
}

void inputsMenu(){
	if(gb.buttons.pressed(BTN_A)){
		(*menuActions[index]) ();
	}
	
	if(gb.buttons.pressed(BTN_UP))
	{
		index --;
		if (index < 0)
			index = menuMaxIndex;
	}
	
	if(gb.buttons.pressed(BTN_DOWN))
	{
		index ++;
		if (index > menuMaxIndex)
			index = 0;
	}
}

void drawMenu(){
	char b[10];
	gb.display.cursorX = 42 - 6;
	gb.display.cursorY = 5;
	gb.display.setColor(BLACK);
	gb.display.drawFastHLine(0,2,84);
	gb.display.fillRect(0,4,84,7);
	gb.display.drawFastHLine(0,9,84);
	gb.display.setColor(WHITE);
	gb.display.print(F("RUN"));
	
	gb.display.setColor(BLACK);
	for(char i = 0; i < menuMaxIndex+1; i ++)
	{
		gb.display.cursorY = 15 + i * 7;
		gb.display.cursorX = 15;
		
		strcpy_P(b, (char*)pgm_read_word(&(menuStrings[i])));
		gb.display.print(b);
	}
	
	gb.display.fillRect(12,17 + index * 7,2,2);
	partCreate(11,18 + index * 7,0,10,1);
	partUpdate();
	partDraw(gb,0,0);
	/* gb.display.cursorY = 15 + index * 7;
	gb.display.cursorX = 10;
	gb.display.setColor(BLACK);
	
	gb.display.print(F(">")); */
}

/////////////////////////////////////////
//////////////// HIGHSCORE //////////////
/////////////////////////////////////////

void highScoreLoop(){

}
void gameLoop(){
	inputsGame();

	if(!initialize) {
		speedCounter ++;
		if (speedCounter > 20 && GameState == GAME)
		{
			speedCounter = 0;
			_speed ++;
		}
			
		updateBonus();
		updatePlayer();
		updateBlocks();
		partUpdate();
		
		shakeCamera();
		
		drawBonus();
		drawBlocks();
		drawPlayer();
		partDraw(gb, drawOffsetX, drawOffsetY);
		
		drawHud();
	}
	else{
		initGameOver();
	}	
}

void gameOverLoop(){
	gameLoop();
	drawGameOver();
}


// Generate block constants
#define MIN_X 50
#define MAX_X 400
#define MIN_Y 25
#define MAX_Y 45
#define MIN_W 300
#define MAX_W 800
#define MIN_H 1
#define MAX_H 6
void generateBlock(){
  // find the first dead block :
  char index = -1;
  int max_x = 84;
  for (char i=0; i < BLOCK_COUNT; i ++)
  {
    if (blocks[i].w == -1 && index == -1)
      index = i;
    if (blocks[i].x + blocks[i].w > max_x && blocks[i].w != -1)
      max_x = (blocks[i].x) + (int)(blocks[i].w);
  }
  
  if (index != -1)
  {
    blocks[index].x = max_x + random(MIN_X, MAX_X);
    blocks[index].y = random(MIN_Y, MAX_Y);
    blocks[index].w = random(MIN_W, MAX_W);
    blocks[index].h = random(MIN_H, MAX_H);
  }
  
    
}


// Updates all the blokcs in da world
void updateBlocks(){
  for (char i = 0; i< BLOCK_COUNT; i ++){
    
    if(blocks[i].w == -1) //Skip the block if it's dead
      continue;
      
    // Update the position of the block
    if(blocks[i].x - _speed > 0)
      blocks[i].x -= _speed;
    else if(blocks[i].x != 0){
     blocks[i].w -= _speed - blocks[i].x;
     blocks[i].x = 0;
    }
    else{
     blocks[i].w -= _speed;
     if (blocks[i].w < 0){
       blocks[i].w = -1;
       generateBlock();
     }
    }
  } 
}

// Draw all the blocks in da world
void drawBlocks(){
  for (char i = 0; i< BLOCK_COUNT; i ++){
   if (blocks[i].w != -1 && blocks[i].x <= 840)
      gb.display.fillRect(blocks[i].x/10+drawOffsetX, blocks[i].y+drawOffsetY, blocks[i].w/10, blocks[i].h); 
  }
}
// Update the player (jump and collisions)
void updatePlayer(){
  if (GameState == GAME)
  {
	//test frontal collision
	for(char i = 0; i<BLOCK_COUNT; i ++){
		if (blocks[i].w != -1)
		{
     if(gb.collideRectRect(blocks[i].x/10, blocks[i].y, blocks[i].w/10, blocks[i].h,player.x + _speed/10, player.y, player.s, player.s)){setGameOver();}
           
   }
	}
	// Apply Gravity
  
  // Jump if a is pressed
    
  score ++;
  
  if (player.vy != 0)
	player.bonusScore += 2;
  
  player.vy += GRAV;
  for(char i = 0; i<BLOCK_COUNT; i ++)
  {
   if (blocks[i].w != -1)
   {
     // Collision up and down
     if(gb.collideRectRect(blocks[i].x/10, blocks[i].y, blocks[i].w/10, blocks[i].h,
                        player.x, 
                        (player.vy > 0 ? player.y : player.y + player.vy / 10) , 
                        player.s, 
                        (player.vy > 0 ? player.s + player.vy / 10 : -(player.vy/10) + player.s) ) && GameState == GAME)
     {
      if(player.vy < 0)
      {
       player.y = blocks[i].y + blocks[i].h;
	   
      } 
      else
      {
        player.y = blocks[i].y - player.s;
		player.jump = 0;
	   if (player.bonusScore > 0)
	  {
		score += player.bonusScore * player.bonusMult;
		
		char j = 0;
		while( j <= MSG_COUNT)
		{
			if ((player.bonusScore * player.bonusMult) < pgm_read_word_near(MessageTreshold + j))
				break;
			j++;
		}
		if(j != 0)
		{
			j --;
			char buffer[15];
			strcpy_P(buffer, (char*)pgm_read_word(&(Messages[j])));
			sprintf(message,"%i %s",player.bonusScore * player.bonusMult, buffer);
			messageTimer = 50;
			
		}
	  }
      }
      player.vy = 0;
      
	  partCreate(player.x-1, player.y+1, 0,10,1);
	  
	  
		
	  
	  player.bonusScore = DEFAULT_BONUS;
	  player.bonusMult = 1;
	  
     }
   }
  }
	player.y += player.vy/10;
	if (player.y > 48)
		setGameOver();
	if (GameState == GAMEOVER)
		partCreate(player.x, player.y, 50,50,10);
  }
   else{ // If the player is dead
   if (player.x > -player.s)
	player.x -= _speed;
   }
  
}

#define NAMECHAR_MIN 32 
#define NAMECHAR_MAX 126
char gameOverCharPos = 0;
char highScoreRank = 0;
void inputsGame(){
	static char jmp_btn = 0;
	//PLAYER JUMP
	if (gb.buttons.repeat(BTN_A,0) && player.jump < MAX_JUMPS && GameState == GAME && !jmp_btn){
		player.vy = PLAYER_JUMP;
		jmp_btn = 1;
		player.jump ++;
    }
	if (!gb.buttons.repeat(BTN_A,0))
		jmp_btn = 0;
	
	if (GameState == GAMEOVER){
		if (gb.buttons.pressed(BTN_B))
		{
		initialize = 1;
		saveHighScores();
		}
		
		// Increase Char
		if (gb.buttons.pressed(BTN_UP))
		{
			highScoresNames[highScoreRank][gameOverCharPos] ++;
			if (highScoresNames[highScoreRank][gameOverCharPos]> NAMECHAR_MAX)
				highScoresNames[highScoreRank][gameOverCharPos] = NAMECHAR_MIN;
		}
		if (gb.buttons.pressed(BTN_DOWN))
		{
			highScoresNames[highScoreRank][gameOverCharPos] --;
			if (highScoresNames[highScoreRank][gameOverCharPos]< NAMECHAR_MIN)
				highScoresNames[highScoreRank][gameOverCharPos] = NAMECHAR_MAX;
		}
		if (gb.buttons.pressed(BTN_LEFT))
		{
			gameOverCharPos --;
			if (gameOverCharPos < 0)
				gameOverCharPos = 2;
		}
		if (gb.buttons.pressed(BTN_RIGHT))
		{
			gameOverCharPos ++;
			if (gameOverCharPos > 2)
				gameOverCharPos = 0;
		}
		
		if (gb.buttons.pressed(BTN_C))
		{
			saveHighScores();
			initMainMenu();
		}
	}
	else if(gb.buttons.pressed(BTN_C))
		initMainMenu();
	
}
void drawPlayer(){
  gb.display.fillRect(player.x+drawOffsetX, player.y+drawOffsetY, player.s, player.s);
}

void shakeCamera(){
	static long cameraTime = gb.frameCount;
	int delta = gb.frameCount - cameraTime;
	if (GameState == GAMEOVER && delta < 50)
	{
		drawOffsetX = random(-2,2);
		drawOffsetY = random (-2,2);
	}
	else if(GameState != GAMEOVER)
	{
		cameraTime = gb.frameCount;
	}
}

#define BONUS_SPAWN_TRESHOLD 20
#define ODD_PER_FRAME 6
#define BONUS_MINH 20
#define BONUS_MAXH 43
void generateBonus(){
	static int lastBonusGen = 0;
	if (random(0,lastBonusGen) > BONUS_SPAWN_TRESHOLD && random(0,ODD_PER_FRAME+1)==ODD_PER_FRAME)
	{
		for(int i = 0; i < BONUS_COUNT; i++)
		{
			if (bonuses[i].type == NONE)
			{
				
				switch (random(1,BONUS_MAX+1)){
					case 1: bonuses[i].type = MORE_JUMPS; break;
					case 2: bonuses[i].type = MULTI; break;
					case 3: bonuses[i].type = SCOREBONUS; break;
					case 4: bonuses[i].type = SLOW; break;
					default: bonuses[i].type = NONE; break;
				}
				bonuses[i].x = 840;
				bonuses[i].y = random(BONUS_MINH, BONUS_MAXH);
				
				
				
				lastBonusGen = 0;
				break;
			}
		}
	}
	lastBonusGen ++;
}

void updateBonus(){
	generateBonus();
	for(int i = 0; i < BONUS_COUNT; i++){
		if (bonuses[i].type != NONE){
		
			// move the bonus out of the blocks
			for(char j = 0; j<BLOCK_COUNT; j ++){
					if (blocks[i].w != -1){
						if(gb.collideRectRect(
										blocks[j].x, blocks[j].y, blocks[j].w, blocks[j].h,bonuses[i].x, bonuses[i].y, BONUS_WIDTH, BONUS_HEIGHT)){
											bonuses[i].y = blocks[j].y - 7;
										}
						}
				}
			
			if(gb.collideRectRect(
					player.x, player.y, player.s, player.s,
					bonuses[i].x/10, bonuses[i].y, BONUS_WIDTH, BONUS_HEIGHT)){
						//create particles
						partCreate(bonuses[i].x/10, bonuses[i].y, 50,25,10);
						
						switch (bonuses[i].type){
							case MORE_JUMPS: 
								player.jump = 0; 
								break;
							case MULTI: 
								player.bonusMult ++ ;
								if (player.bonusScore < 0)
									player.bonusScore = 0;
								break;
							case SCOREBONUS: 
								player.bonusScore += 100; 
								break;
							case SLOW: 
								if (_speed > 20)
									_speed -= 5; 
								break;
						}
				
				
						bonuses[i].type = NONE;
					}
			}
		bonuses[i].x -= _speed;
		if (bonuses[i].x < 0)
			bonuses[i].type = NONE;
		}			
}


void drawBonus(){
	for(int i = 0; i < BONUS_COUNT; i++){
		if (bonuses[i].type != NONE)
			{
				gb.display.cursorX = bonuses[i].x/10 + drawOffsetX;
				gb.display.cursorY = bonuses[i].y + drawOffsetY;
				char c = '0';
				switch (bonuses[i].type){
					case MORE_JUMPS: c = 4 ; break;
					case MULTI: c = '*'; break;
					case SCOREBONUS: c = '+'; break;
					case SLOW: c = 'S'; break;
					default: c = ' '; break;
				}
				gb.display.print(c);
			}
	}
}

void drawHud(){
	// Display messages
	if (messageTimer>0){
		messageTimer --;
		gb.display.cursorY = 7;
		if (messageTimer < 7)
			gb.display.cursorY = messageTimer;
		gb.display.cursorX = 0;
		gb.display.print(message);
	}
	// Draw a white rectangle in the hud zone
	gb.display.setColor(WHITE);
	gb.display.fillRect(0,0,84,6);
	gb.display.setColor(BLACK);
	// DRAW THE SCORE AND BONUSES
	gb.display.cursorX = 0;
	gb.display.cursorY = 0;
	char b[5];
    sprintf(b,"%05d",score);
    gb.display.print(b);
	if (player.bonusScore > 0)
	{
		sprintf(b,"+%04d",player.bonusScore);
		gb.display.print(b);
		if (player.bonusMult > 1)
		{
			sprintf(b,"X%01d",player.bonusMult);
			gb.display.print(b);
		}
	}
	
	// DRAW THE JUMP METER
	gb.display.cursorX = (84-(4*4));
	gb.display.print(F("JUMP"));
	gb.display.drawFastVLine(84-18,0,6);
	gb.display.setColor(INVERT);
	gb.display.fillRect(84-16-1,0,16-player.jump*16/MAX_JUMPS,6);
	gb.display.setColor(BLACK);
	
	gb.display.drawFastHLine(0,5,84-18);
	gb.display.drawFastHLine(84-17,6,17);
	
	#ifdef DEBUG
	gb.display.cursorX = 48;
	gb.display.cursorY = 7;
	gb.display.print(gb.getCpuLoad());
	gb.display.print(',');
	gb.display.print(gb.frameDurationMicros);
	#endif
}

void drawGameOver(){
	gb.display.fillRect(0,20,84,7);
	gb.display.setColor(WHITE);
	gb.display.cursorX = (24);
	gb.display.cursorY = 21;
	gb.display.print(F("GAME OVER"));
	gb.display.setColor(BLACK);
	gb.display.cursorX = (6);
	gb.display.cursorY = 14;
	if (highScoreRank == 3)
		gb.display.print(F("Press B to restart"));
	else
	{
		gb.display.cursorX = 15+random(-1,1);
		gb.display.cursorY = 14 + random(-1,1);;
		gb.display.print(F("HIGH SCORE"));
	}
		
	gb.display.setColor(WHITE);
	gb.display.fillRect(22,28,40,28+16);
	gb.display.setColor(BLACK);
	for(char i = 0; i<3; i++)
	{
		gb.display.cursorX = (23);
		gb.display.cursorY = 28+i*6;
		gb.display.print(highScoresNames[i]);
		gb.display.print(' ');
		gb.display.print(highScoresScore[i]);
	}
	if(highScoreRank!=3 && gb.frameCount%20 > 9){
		gb.display.setColor(INVERT);
		gb.display.fillRect(22+4*gameOverCharPos,28+highScoreRank*6,5,5);
	}
	
	
}


void setGameOver(){
	highScoreRank = 0;
	highScoreRank = 3;
	for(char i = 0; i<3; i++)
	{
		highScoresScore[i] = readHSCScore(i);
		readHSCName(highScoresNames[i],i);
	}
	
	//check if high score
	char i = 2;
	while( i>=0 && highScoresScore[i] < score)
	{	
		i--;
	}
	i++;
	if(i<3) // If there is a new higscore !
	{
		for( char j = 2; j > i; j--)
		{
			highScoresScore[j] = highScoresScore[j-1];
			highScoresNames[j][0] = highScoresNames[j-1][0];
			highScoresNames[j][1] = highScoresNames[j-1][1];
			highScoresNames[j][2] = highScoresNames[j-1][2];
		}
		highScoresScore[i] = score;
		highScoreRank = i;
		
		
	}
	
	player.bonusScore = -25;
	player.bonusMult = 0;
	GameState = GAMEOVER;
}

#define GAME_ID 0x01FF
char isValidHSC(){
	int valid = ((((int)EEPROM.read(0)) << 8) + (int)EEPROM.read(1));
	return valid == GAME_ID;
}

void saveHighScores()
{
	if (highScoreRank != 3){
		for(char j = highScoreRank; j < 3; j++)
		{
			writeHSC(highScoresNames[j],highScoresScore[j],j);
		}
	}
		
}

void initHighScore(){
	if (!isValidHSC()){
		EEPROM.write(0,GAME_ID>>8);
		EEPROM.write(1,GAME_ID);
		writeHSC("AAA",00000,0);
		writeHSC("AAA",00000,1);
		writeHSC("AAA",00000,2);
	}
}

void writeHSC(char name[4], int score,char pos){
	pos *= 5;
	EEPROM.write(pos+HSC_OFFSET,name[0]);
	EEPROM.write(pos+1+HSC_OFFSET,name[1]);
	EEPROM.write(pos+2+HSC_OFFSET,name[2]);
	EEPROM.write(pos+3+HSC_OFFSET,score>>8);
	EEPROM.write(pos+4+HSC_OFFSET,score);
}

unsigned int readHSCScore(char pos){
	pos *= 5;
	return ((((int)EEPROM.read(pos+3+HSC_OFFSET)) << 8) + (int)EEPROM.read(pos+4+HSC_OFFSET));
}

void readHSCName(char* buffer,char pos){
	pos *= 5;
	buffer[0] = EEPROM.read(pos+HSC_OFFSET);
	buffer[1] = EEPROM.read(pos+1+HSC_OFFSET);
	buffer[2] = EEPROM.read(pos+2+HSC_OFFSET);
	buffer[3] = '\0';
}